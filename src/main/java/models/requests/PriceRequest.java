package models.requests;

import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PriceRequest {

    @NotNull(message = "Instrument is required")
	@JsonProperty("instrument")
	private String instrument;

	@NotNull(message = "Bid is required")
	@JsonProperty("bid")
	private String bid;

	@NotBlank(message = "Ask is required")
	@JsonProperty("ask")
	private String ask;

	@NotNull(message = "Timestamp is required")
	@JsonProperty("timestamp")
	private String timestamp;
}
