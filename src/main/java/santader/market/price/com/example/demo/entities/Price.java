package santader.market.price.com.example.demo.entities;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;

@Data
@Entity
public class Price {
    
	/**
	 * Unique identifier of price.
	 * 
	 * @return the current identifier of price
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @CsvBindByName
	private Long id;

	/**
	 * Instrument name of price.
	 * 
	 * @param instrumentName the new instrument for price.
	 * @return the current price instrument.
	 */
    @CsvBindByName(column = "instrument")
	private Instrument instrumentName;

	/**
	 * Bid of price.
	 * 
	 * @param bid the new value for sell price.
	 * @return the current value of sell price.
	 */
    @CsvBindByName
	private Double bid;

    /**
	 * Ask of price.
	 * 
	 * @param ask the new value for buy price.
	 * @return the current value of buy price.
	 */
    @CsvBindByName
	private Double ask;

    /**
	 * Timestamp of price.
	 * 
	 * @param timestamp the new timestamp price.
	 * @return the current value of price timestamp.
	 */
    @CsvBindByName
	private LocalDateTime timestamp;
}
