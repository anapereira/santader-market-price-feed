package santader.market.price.com.example.demo.controllers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import models.requests.PriceRequest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PriceController {
    
    @GetMapping("/prices")
    public List<List<String>> getAll() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        Reader reader = Files.newBufferedReader(Paths.get(classLoader.getResource("market_price_feed.csv").toURI()));

        List<List<String>> records = new ArrayList<List<String>>();
        try (CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build()) {
            String[] values = null;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));
            }
        }
        return records;
    }


    @PostMapping("/prices")
    @ResponseStatus(HttpStatus.CREATED)
    public String store(@Valid @RequestBody PriceRequest request) throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();

        try{
            List<List<String>> records = this.getAll();

            List<String> record = records.get(records.size() -1);   
            FileWriter csvwriter = new FileWriter(classLoader.getResource("market_price_feed.csv").getFile(), true);
            String id = Integer.toString(Integer.parseInt(record.get(0)) + 1);
            csvwriter.append(id);
            csvwriter.append(",");
            csvwriter.append(request.getInstrument());
            csvwriter.append(",");
            csvwriter.append(request.getBid());
            csvwriter.append(",");
            csvwriter.append(request.getAsk());
            csvwriter.append(",");
            csvwriter.append(request.getTimestamp());
            csvwriter.append("\n");
            csvwriter.close();
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }

        return "With Success!";
    }
    

    @GetMapping("/prices/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void GetMessage(@PathVariable("id") Integer id) throws Exception {
    
        List<List<String>> records = this.getAll();

        
        for (int i = 0; i < records.size(); i++) {
            if (records.get(i).contains(Integer.toString(i))) {
               System.out.println("Result:" + records.get(i));
            }
        }

    }
    
}

