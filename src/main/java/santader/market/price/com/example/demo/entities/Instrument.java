package santader.market.price.com.example.demo.entities;

public enum Instrument {
	EUR_USD, EUR_JPY, GBP_USD
}
