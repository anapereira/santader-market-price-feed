# README #

This README would normally document whatever steps are necessary to get your application up and running.

## How to start

To run the application in development you have to execute the following commands:

# Then, run the application
```
mvn spring-boot:run
```

### The Endpoints: ###
```
http://localhost:8080/prices
```
Get all market prices from CSV

```
http://localhost:8080/prices
```
Store a new price on CSV

E.g:
```
{
    "instrument": "USD/EUR",
    "bid": "1000.00",
    "ask": "2000.00",
    "timestamp" : "01-06-2020 12:01:02:100"
}
```

```
http://localhost:8080/prices/{id}
```
Get Specific price by id 
